import java.util.Scanner;
public class TicTacToeGame{
	public static void main(String[] args){
		System.out.println("Welcome to my Tic Tac Toe Game!");
		Board playground = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		Scanner reader = new Scanner(System.in);
		while (!gameOver){
		System.out.println(playground);
		if (player == 1){
		playerToken = Square.X;	
		}
		else if (player == 2){
		playerToken = Square.O;		
		}
		System.out.println("Player " + player + ", it's your turn!");
		boolean aValidPlace = false;
		while (!aValidPlace){
		    System.out.println("Enter the row where you want to place your token (1-3): ");
		    int row = reader.nextInt() - 1;
		    System.out.println("Enter the column where you want to place your token (1-3): ");
		    int col = reader.nextInt() - 1;
			if (playground.placeToken(row, col, playerToken)){
				aValidPlace = true;
			}
			else {
				System.out.println("Invalid input, please try again");
			}
		}
		if (playground.checkIfWinning(playerToken)){
			System.out.println(playground);
			System.out.println("Player " + player + " wins!");
			gameOver = true;
		}
		else if (playground.checkIfFull()){
			System.out.println(playground);
			System.out.println("It's a tie!");
			gameOver = true;
		}
		else {
			player++;
			if (player > 2){
				player = 1;
			}
		}
		
		}
	}
}