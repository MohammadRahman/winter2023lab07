public class Board{
	//Field
	private Square[][] tictactoeBoard;
	//Getter
	public Square[][] getTictactoeBoard(){
		return this.tictactoeBoard;
	}
	//Constructor
	public Board(){
		tictactoeBoard = new Square[3][3];
        for (int row = 0; row < tictactoeBoard.length; row++) {
            for (int col = 0; col < tictactoeBoard[row].length; col++) {
                tictactoeBoard[row][col] = Square.BLANK;
			}
		}
	}
	//toString
	public String toString(){
		String value = "";
		for (int rows = 0; rows < this.tictactoeBoard.length; rows++){
		for (int col = 0; col < this.tictactoeBoard[rows].length; col++){
		value += this.tictactoeBoard[rows][col] + " ";
		}
		value += "\n";
		}
		return value;
	}
	//Instance Method
	public boolean placeToken(int row, int col, Square playerToken){
		if (row < 0 || row > 3 || col < 0 || col > 3){
			return false;
		}
		if(this.tictactoeBoard[row][col] == Square.BLANK){
			this.tictactoeBoard[row][col] = playerToken;
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkIfFull(){
		for (int rows = 0; rows < this.tictactoeBoard.length; rows++){
		for (int col = 0; col < this.tictactoeBoard[rows].length; col++){
		if (this.tictactoeBoard[rows][col] == Square.BLANK){
			return false;
		}
		}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for (int rows = 0; rows < this.tictactoeBoard.length ; rows++){
			if (this.tictactoeBoard[rows][0] == playerToken && this.tictactoeBoard[rows][1] == playerToken &&
			this.tictactoeBoard[rows][2] == playerToken){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
		for (int col = 0; col < this.tictactoeBoard.length ; col++){
			if (this.tictactoeBoard[0][col] == playerToken && this.tictactoeBoard[1][col] == playerToken &&
			this.tictactoeBoard[2][col] == playerToken){
				return true;
			}
		}
		return false;
	}
	private boolean checkIfWinningDiagonal(Square playerToken){
		if ((this.tictactoeBoard[0][0] == playerToken && this.tictactoeBoard[1][1] == playerToken &&
			this.tictactoeBoard[2][2] == playerToken) || (this.tictactoeBoard[0][2] == playerToken && this.tictactoeBoard[1][1] == playerToken &&
			this.tictactoeBoard[2][0] == playerToken)){
				return true;
			}
			return false;
	}
	
	public boolean checkIfWinning(Square playerToken){
		if (checkIfWinningHorizontal(playerToken)|| checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken)){
			return true;
		}
        return false;		
	}
}